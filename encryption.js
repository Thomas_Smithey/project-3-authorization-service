export default function encrypt(password = "")
{
    let encrypted = password.split("").map(a=>a.charCodeAt(0));
    const chars = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
    const delimiters = ["!", "@", "#", "$", "%", "^", "&", "*"];

    // get values of array to binary
    for (let i = 0; i < encrypted.length; i++)
    {
        encrypted[i] = encrypted[i].toString(2);
        // console.log(`# ${i}: ${encrypted[i]}`);

        encrypted[i] = encrypted[i].split(""); // to array of chars.
        // characters are 1s, all other letters are 0s.
        
        for (let j = 0; j < encrypted[i].length; j++)
        {
            if (encrypted[i][j] === "1")
            {
                // replace with an alphabetic character.
                const index = Math.floor(Math.random() * 26);
                encrypted[i][j] = chars[index];
            }
            else
            {
                // replace with numeric character.
                // Math.floor(Math.random() * 10); 0-9
                encrypted[i][j] = Math.floor(Math.random() * 10);
            }

            if (j === encrypted[i].length - 1)
            {
                const index = Math.floor(Math.random() * 8);
                encrypted[i][j] += (delimiters[index]);
            }
        }
        encrypted[i] = encrypted[i].join("");
    }
    encrypted = encrypted.join(""); // fully encrypted string at this point.
    return encrypted;
}