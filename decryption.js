export default function decrypt(encrypted = "")
{
    const delimiters = ["!", "@", "#", "$", "%", "^", "&", "*"];
    let decrypted = [];
    let tempString = "";
    let delimiterFlag;
    for (let i = 0; i < encrypted.length; i++)
    {
        delimiterFlag = false;

        for (let j = 0; j < delimiters.length; j++)
        {
            if (encrypted[i] === delimiters[j])
            {
                decrypted.push(tempString);
                tempString = "";
                delimiterFlag = true;
            }
        }

        if (!delimiterFlag)
        {
            tempString += encrypted[i];
        }
    }

    // Now we have an array where each element is the obfuscated binary number: a string where alphabetic characters represent ones
    // and all numbers represent zeroes.
    // example: [ 'qu2163e', 'xwc54fi', 'ic20e39', 'qd407w', 'cw88a7', 'al51cb' ]

    for (let i = 0; i < decrypted.length; i++)
    {
        decrypted[i] = decrypted[i].split("");
        for (let j = 0; j < decrypted[i].length; j++)
        {
            if (Number(decrypted[i][j]).toString() === "NaN") // if the character is non-numeric
            {
                decrypted[i][j] = "1";
            }
            else
            {
                decrypted[i][j] = "0";
            }
        }
    }

    for (let i = 0; i < decrypted.length; i++)
    {
        decrypted[i] = decrypted[i].join("");
        decrypted[i] = String.fromCharCode(parseInt(decrypted[i], 2));
    }

    decrypted = decrypted.join("");
    return decrypted;
}